package Projects.CodistanProjects.Java1Tasks;

import org.testng.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
@Listeners(Projects.CodistanProjects.Java1Tasks.Task003.class)
public class Task003TestCase {

	@Test

	public void Task3() {
		TaskUtil.starter("https://www.amazon.com/");
		System.out.println(TaskUtil.getDriver().getTitle());
		TaskUtil.getDriver().quit();
		
		String expectedTitle = "Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more";
		Assert.assertEquals("Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more", expectedTitle);
		
	}
}
