package Projects.CodistanProjects.Java1Tasks;

import org.testng.ITestListener;
import org.testng.ITestResult;

public class Task003 implements ITestListener{

	public void onTestStart(ITestResult result) {//for method
	System.out.println("Starting Test: "+result.getName());
	}
	public void onTestSuccess(ITestResult result) {
	System.out.println("Test case passed: "+result.getName());
	}
	public void onTestFailure(ITestResult result) {
	System.out.println("Test case failed: "+result.getName());
	}
	public void onTestSkipped(ITestResult result) {

	System.out.println("Test case skipped: "+result.getName());
	}
	
}
