package Projects.CodistanProjects.Java1Tasks;

public class Task011 {

	final double PI;

	Task011() {
		this.PI = 3.14159265359;
	}

	public static void main(String[] args) {
		Task011 t11 = new Task011();
		System.out.println(t11.PI);
	}
}
