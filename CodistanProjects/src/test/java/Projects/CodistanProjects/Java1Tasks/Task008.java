package Projects.CodistanProjects.Java1Tasks;

public abstract class Task008 {
	
	public Task008(String accountNo, String routingNo, String ownersFullName, double balance) {
		super();
		this.accountNo = accountNo;
		this.routingNo = routingNo;
		this.ownersFullName = ownersFullName;
		this.balance = balance;
	}
	
	String accountNo; 
	String routingNo; 
	String ownersFullName; 
	private double balance; 
	abstract protected void depositCash(double amount);
	
	abstract protected void withdrawMoney(double amount);
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	
}
