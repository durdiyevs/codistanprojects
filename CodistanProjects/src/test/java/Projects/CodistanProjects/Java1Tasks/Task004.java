package Projects.CodistanProjects.Java1Tasks;

import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebElement;

public class Task004 {
  
	//In your framework, overload at least 2 methods and write the locations of them
	public static void switchToFrame(String idOrName) {
		try {
		TaskUtil.getDriver().switchTo().frame(idOrName);
		} catch (NoSuchFrameException e) {
		System.out.println("Frame is not present");
		}
		}
		public static void switchToFrame(WebElement element) {
		try {
	    TaskUtil.getDriver().switchTo().frame(element);
		} catch (NoSuchFrameException e) {
		System.out.println("Frame is not present");
		}
		}
		public static void switchToFrame(int index) {
		try {
		TaskUtil.getDriver().switchTo().frame(index);
		} catch (NoSuchFrameException e) {
		System.out.println("Frame is not present");

		}
		
		
		
		}
}
