package Projects.CodistanProjects.Java1Tasks;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import io.github.bonigarcia.wdm.WebDriverManager;

    public class TaskUtil {

	public static WebDriver driver = null;
	public Actions act;

	public TaskUtil() {

		
		driver.quit();
	}

	public static void starter(String url) {

		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(url); // "http://newtours.demoaut.com"
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

	}

	public static String getScreenShot() {
		String date = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date(0));
		// not to make my screenshot img duplicate
		TakesScreenshot ts = (TakesScreenshot) TaskUtil.getDriver();
		File source = ts.getScreenshotAs(OutputType.FILE);
		String target = System.getProperty("user.dir") + ("test-output/Midlevel" + date + ".png"); // C:\Users\Suleyman
		File finalDestination = new File(target);
		try {
			FileUtils.copyFile(source, finalDestination);
		} catch (IOException e) {
			System.out.println("Something went wrong! ");
			e.printStackTrace();
		} // D\eclipse-workspace\TestNGPar
			// user.dir// It is not for flow of application
		return target;
	}

	public static WebDriver getDriver() {
		return driver;
	}

//	public static void startApplication() {
//		if (driver == null) 
//
//			// MidlevelUtility Utulity = new MidlevelUtility();
//
//		}

	public static void closeApplication() {
		if (driver != null) {

			TaskUtil utility = new TaskUtil();

		}

	}

	public static void click(String xpath) {
		WebElement locater = driver.findElement(By.xpath(xpath));
		locater.click();

	}

}
