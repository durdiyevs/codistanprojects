package Projects.CodistanProjects.Java1Tasks;

import java.io.FileInputStream;
import java.util.Properties;
import java.util.Scanner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Task002 {

	public static void main(String[] args) {
		
		//Final Example

		double fahrenheit = 25;

		final double celsius = ((5 * (fahrenheit - 32.0)) / 9.0);
		System.out.println(fahrenheit + " degree Fahrenheit is equal to " + celsius + " in Celsius");
        testFinalKey();
		

	}public static final void testFinalKey() {
		System.out.println("Parent final method");
	}
	
	
	
	
	
	
	
	//Finally Example

	public Task002() {

		PageFactory.initElements(TaskUtil.getDriver(), this);

	}

	@FindBy(xpath = "//button[text()='Double-Click Me To See Alert']")
	public WebElement locater;

	@FindBy(xpath = "//button[@id='dblClkBtn']")
	public WebElement locater2;

	public final void isSelectedMethod(String path) {
		try {
			boolean element = TaskUtil.driver.findElement(By.xpath(path)).isSelected();
			if (element == true) {
				System.out.println("Element is Selected ! ");
			} else {
				System.out.println("Element is not Selected ! ");
			}
		} catch (Exception e) {
			System.out.println("Element Not Found!");

		} finally {
			System.out.println("Finally block is Executed");
		}

	}
	

	}


