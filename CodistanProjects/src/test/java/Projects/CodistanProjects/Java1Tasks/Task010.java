package Projects.CodistanProjects.Java1Tasks;
//In your framework, share your code that has protected keyword
public class Task010 extends Task008{

	
	public Task010(String accountNo, String routingNo, String ownersFullName, double balance) {
		super(accountNo, routingNo, ownersFullName, balance);

	}

	protected void depositCash(double amount) {

		super.setBalance(getBalance() + amount);
	}

	public void depositCheck(double amount) {
		super.setBalance(getBalance() + amount);

	}

	public void withdrawMoney(double amount) {

		if (getBalance() - amount >= -1000) {
			super.setBalance(getBalance() - amount);
			if(getBalance()<0) {
				super.setBalance(getBalance() - 45);
			}
		} 
		else {
			System.out.println("Your balance can not be less than -$1000, please revise your amount.");
		}

	}

	public void setBalance(double balance) {
		System.out.println("Setting Balance is not allowed.");
	}
}
