package Projects.CodistanProjects.Java1Tasks;

import java.util.HashSet;
import java.util.Iterator;

public class Task026 {
	public static void main(String[] args) {
		HashSet hset = new HashSet();
		hset.add("apple");

		hset.add("samsung");
		// How to print all values from hashSet
		// 1. advance loop
		for (Object obj : hset) {
			System.out.println(obj);
		}
		// 2. using iterator
		Iterator itr = hset.iterator();
		while (itr.hasNext()) {
			Object words = itr.next();
			System.out.println(words);

		}
	}
}
