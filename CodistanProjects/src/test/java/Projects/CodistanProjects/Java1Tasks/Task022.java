package Projects.CodistanProjects.Java1Tasks;

import java.util.TreeSet;

public class Task022 {
public static void main(String[] args) {
    //Creating a TreeSet without supplying any Comparator
	 
    TreeSet<Integer> set = new TreeSet<Integer>();

    //Adding elements to TreeSet

    set.add(23);      

    set.add(11);    

    set.add(41);      

    set.add(7);

    set.add(69);

    set.add(18);

    set.add(38);

    //printing elements of TreeSet

    System.out.println(set);      //Output : [7, 11, 18, 23, 38, 41, 69]
}
}

