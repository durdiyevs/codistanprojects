package Projects.CodistanProjects.Java1Tasks;

import java.util.Iterator;
import java.util.LinkedList;

public class Task019 {

	public static void main(String[] args) {

		LinkedList<String> ll = new LinkedList();

		// add::

		ll.add("test");
		ll.add("qtp");
		ll.add("selenium");
		ll.add("RPA");
		ll.add("RFT");

		// print:
		System.out.println("content of linkedlist:" + ll);
		// addfirst:
		ll.addFirst("Tom");
		// addLast:
		ll.addLast("Automation");
		System.out.println("content of linkedlist:" + ll);

		// get:
		System.out.println(ll.get(0));
		// set:
		ll.set(0, "Tom"); // Insert Element
		System.out.println(ll.get(0));

		// remove first and last element:
		ll.removeFirst();
		ll.removeLast();
		System.out.println("content of LinkedList" + ll);

		ll.remove(2);
		System.out.println("content of LinkedList" + ll);

		// how to print all the values of LinkedList:
		// for loop

		System.out.println("**using for loop");
		for (int i = 0; i < ll.size(); i++) {
			System.out.println(ll.get(i));
		}

		// advanced for loop
		System.out.println(" ***using advanced for loop");
		for (String str : ll) {
			System.out.println(str);
		}

		System.out.println("**using iterator");
		Iterator<String> it = ll.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());

		}

		// while loop
		System.out.println("***using while loop");
		int num = 0;
		while (ll.size() > num) {
			System.out.println(ll.get(num++));

		}
	}
}
