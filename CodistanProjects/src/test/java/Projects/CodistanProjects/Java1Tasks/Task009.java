package Projects.CodistanProjects.Java1Tasks;

public class Task009 {

	
	// finalize is not a reserved keyword.
public static void main(String[] args) {
	Task009 t9 = new Task009();
	t9.finalize();
	 t9.finalize(); 
     t9 = null;
}

public void finalize() {
	System.out.println("finalize method overriden");
}
}
