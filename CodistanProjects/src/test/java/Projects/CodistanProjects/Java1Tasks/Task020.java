package Projects.CodistanProjects.Java1Tasks;

import java.util.LinkedList;

public class Task020 {
	public static void main(String[] args) {
		LinkedList linkedList = new LinkedList();
		// Add elements
		linkedList.add("A");

		linkedList.add("B");
		System.out.println(linkedList);
		// Add elements at specified position
		linkedList.add(2, "C");
		linkedList.add(3, "D");
		System.out.println(linkedList);
		// Remove element
		linkedList.remove("A"); // removes A
		linkedList.remove(0); // removes B
		System.out.println(linkedList);
	}
}
