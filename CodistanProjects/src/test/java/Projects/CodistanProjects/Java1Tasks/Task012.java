package Projects.CodistanProjects.Java1Tasks;

import java.io.FileInputStream;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Task012 {

	XSSFWorkbook wbook;
	XSSFSheet sheet;

	public Task012(String excelPath) {

		try {
			FileInputStream fis = new FileInputStream(excelPath);

			wbook = new XSSFWorkbook(fis);

			sheet = wbook.getSheetAt(0);

		} catch (Exception e) {

			System.out.println(e.getMessage());

		}finally {
			System.out.println("Finally block is Executed");
		}
	}
}
