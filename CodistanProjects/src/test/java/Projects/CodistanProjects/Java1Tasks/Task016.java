package Projects.CodistanProjects.Java1Tasks;

import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Task016 {
	static WebDriver driver;

	public static void switchToFrame(String idOrName) {
		try {
		driver.switchTo().frame(idOrName);
		} catch (NoSuchFrameException e) {
		System.out.println("Frame is not present");
		}
		
		}
		public static void switchToFrame(WebElement element) {
		try {
		driver.switchTo().frame(element);
		} catch (NoSuchFrameException e) {
		System.out.println("Frame is not present");
		}
		
		}
		public static void switchToFrame(int index) {
		try {
		driver.switchTo().frame(index);
		} catch (NoSuchFrameException e) {
		System.out.println("Frame is not present");

		}
}
}