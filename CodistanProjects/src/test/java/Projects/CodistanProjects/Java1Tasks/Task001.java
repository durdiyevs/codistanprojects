package Projects.CodistanProjects.Java1Tasks;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Task001 {
	
	public Task001() {
		PageFactory.initElements(TaskUtil.getDriver(), this);
	}
	@FindBy(xpath="//img[contains(@src, 'logo')]")
    WebElement logo;
	
	
	
	
	

    void defaultAccessMod() { 
        System.out.println("This is default Access Modifier!"); 
    } 
    
    
    private void displayAccessMod() { 
        System.out.println("This is display Access Modifier!"); 
    } 

    protected void protecteAccessMod() { 
        System.out.println("This is protected Access Modifier!"); 
    } 
    
    public void publicAccessMod() 
    { 
        System.out.println("This is public Access Modifier!"); 
    } 
    
    public static void main(String[] args) {
		Task001 t = new Task001();
		t.defaultAccessMod();
		t.displayAccessMod();
		t.protecteAccessMod();
		t.publicAccessMod();
	}
}
