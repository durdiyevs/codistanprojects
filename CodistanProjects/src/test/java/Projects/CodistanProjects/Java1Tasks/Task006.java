package Projects.CodistanProjects.Java1Tasks;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;




//In your framework, overload at least 2 constructors and write the locations of them
public class Task006 {
	@FindBy(xpath = "//button[text()='Double-Click Me To See Alert']")
	public WebElement locator;
	@FindBy(xpath = "//input[@id='twotabsearchtextbox']")
	public WebElement locater2;
	
	//double click
	public Task006(WebDriver driver, WebElement locator) {
		Actions doubleC = new Actions(driver);
		doubleC.doubleClick(this.locator).perform();
	
	}

	public Task006(String str) {
		Actions action = new Actions(TaskUtil.getDriver());
		  action.sendKeys(this.locater2,str).perform();
	}
	



	public Task006() {

		PageFactory.initElements(TaskUtil.getDriver(), this);

	}




}