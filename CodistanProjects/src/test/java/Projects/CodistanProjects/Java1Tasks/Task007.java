package Projects.CodistanProjects.Java1Tasks;

public class Task007 {

	//static member holds only one instance of the singleton class
	private static Task007 singletonInstance;
	//creating private constructor to prevent instantiation
	private Task007(){

	}
	//create public method to return an instance of the class
	public static Task007 getInstance() {
	singletonInstance=new Task007();
	return singletonInstance;
	}
}
