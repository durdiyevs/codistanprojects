package Projects.CodistanProjects.Java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC014 {

	
	@Test
	public void TC014() {
		
		String str = "Hello World";
		String result = str.replace("o", "a");
		System.out.println(result);
		
		String expected = "Hella Warld";
		Assert.assertEquals(result, expected );
	}
}
