package Projects.CodistanProjects.Java1TestCases;

import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC006 {

	@Test
	public void TC005() {
		ArrayList<Integer> myInt = new ArrayList<Integer>(10);
		myInt.add(3);
		myInt.add(5);
		myInt.add(1);
		myInt.add(7);
		myInt.add(9);
		Collections.reverse(myInt);
		System.out.println(myInt);
  
		int[] expected = {9, 7, 1, 5, 3};
		Assert.assertEquals(myInt, expected);
	}
}
