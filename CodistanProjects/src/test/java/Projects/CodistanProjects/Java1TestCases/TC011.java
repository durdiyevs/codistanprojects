package Projects.CodistanProjects.Java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC011 {
	@Test
	public void TC011() {
		String str = "Hello World";
		
		
		int firstIndex = str.indexOf('o');
		System.out.println("First occurrence of char 'o'" +  " is found at : " + firstIndex);
		
		Assert.assertEquals(firstIndex, 4);
		
		
	}
}
