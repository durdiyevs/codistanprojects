package Projects.CodistanProjects.Java1TestCases;

import static org.testng.Assert.assertEquals;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC013 {

	@Test
	public void TC013() {
	/*
	 * Given two Strings as "Cat" and "Crocodile"
	 * When compare the character numbers to see if they are equal
	 * Then you will get false
	 */
	
	String str1= "Cat";
	String str2="Crocodile";
	
	

	System.out.println("Result is: " + (str1.length() == str2.length() ));
	
	Assert.assertNotEquals(str1.length(), str2.length());
	}
	
}
