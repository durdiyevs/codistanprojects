package Projects.CodistanProjects.Java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC004 {
	@Test
	public void TC004() {
		/*
		 * Given Integer myInteger = 55, Integer myInteger2 = 10
		 * When we divide myInteger by myInteger2
		 * Then we should see the result 5.5 in float.
		 */
		int myInt = 55;
		int myInt2 = 10;	
		float flt = (float)myInt/myInt2;
		System.out.println(flt);
		Assert.assertEquals(flt, 5.5);
		
		
				
		
		
	}

}
