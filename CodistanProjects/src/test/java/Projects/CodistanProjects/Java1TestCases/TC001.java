package Projects.CodistanProjects.Java1TestCases;

import static org.testng.Assert.assertEquals;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC001 {

	@Test
	public void TC001() {
		int a = 5;
		int b =7;
		int total = a+b;
		Assert.assertEquals(total, 12);
	}
	
}
