package Projects.CodistanProjects.Java1TestCases;

import javax.sound.midi.Soundbank;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC002 {
	/*
	 * Given Euro-Dollar conversion rate is $1=0.9 Euro
	 * When the dollar in the amound of 69 in string, 
	 * Then the euro conversion should be 62.23
	 */
	@Test
	public void TC002() {
		
		double Euro = 0.9;
		double Dollar = 1;

		double Conversion = (Euro * Dollar)*69;
		System.out.println("Euro to Dollar conversion is: " + Conversion);
		
		System.out.println(Conversion);
		Assert.assertNotEquals(Conversion, 62.23);

	}
}
