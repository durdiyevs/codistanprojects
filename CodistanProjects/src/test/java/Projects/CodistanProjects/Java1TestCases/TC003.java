package Projects.CodistanProjects.Java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC003 {
	
@Test
public void TC003() {
	/*
	 * Given int a = 55 and int b = 66
	 * When we swap int values
	 * Then int a will be 66 and int b will be 55
	 */
	int a = 55;
	int b= 66;
	int tempVar = a;
	a=b;
	b=tempVar; 
	System.out.println("Value of b: " +b);
	System.out.println("Value of a: " +a);
	Assert.assertEquals(a, 66);
	Assert.assertEquals(b, 55);
	
}
}
