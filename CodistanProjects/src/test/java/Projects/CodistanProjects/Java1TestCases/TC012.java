package Projects.CodistanProjects.Java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC012 {
	@Test
	public void tc012() {
		
		String str1 = "Cat";
		String str2 = "Dog";
		
		
		System.out.println(str1.length() == str2.length());
		
		Assert.assertEquals(str1.length(), str2.length());

	}

}
