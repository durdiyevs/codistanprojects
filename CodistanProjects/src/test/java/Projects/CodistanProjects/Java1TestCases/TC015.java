package Projects.CodistanProjects.Java1TestCases;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC015 {

	@Test
	public void TC015() {
		String str="Hello World";
		
		String actual = str.toUpperCase();
		System.out.println(actual);
		
		String expected= "HELLO WORLD";
		Assert.assertEquals(actual, expected);
	}
}
