package Projects.CodistanProjects.Java1TestCases;

import static org.testng.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collections;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TC005 {

	@Test
	public void TC005() {
		/*
		 * Given an empty array of size 10 When we add, 3, 5, 1, 7, 9 and print the
		 * items in the reverse order Then we should see 9, 7, 1, 5, 3
		 */

		int[] myArray = new int[10];
		myArray[0] = 3;
		myArray[1] = 5;
		myArray[2] = 1;
		myArray[3] = 7;
		myArray[4] = 9;
		System.out.println("Array : " + Arrays.toString(myArray));

		myArray = removeZero(myArray);
		System.out.println("Array after removing zeros : " + show(myArray));

		myArray = reverseArr(myArray);
		System.out.println("Array after reverse: : " + show(myArray));
	

		int[] expected = { 9, 7, 1, 5, 3 };

		Assert.assertEquals(myArray, expected);

	}public static String show(int[] nums) {

		return Arrays.toString(nums);

	}

	// Method to remove zeros
	public static int[] removeZero(int[] nums) {
		int count = 0;
		for (int i = 0; i < nums.length; i++) {

			if (nums[i] != 0) {

				count++;
			}

		}
		int[] result = new int[count];
		for (int i = 0; i < nums.length; i++) {
			if (nums[i] != 0) {
				result[i] = nums[i];

			}
		}
		return result;
	}

	// Method to reverse the Array
	public static int[] reverseArr(int[] nums) {
		int[] reverse = new int[nums.length];
		for (int i = 0; i < nums.length; i++) {
			reverse[i] = nums[nums.length - i - 1];
			// System.out.println("Array after reverse: " + Arrays.toString(reverse));
		}
		return reverse;
	}

	
	//
	
}
